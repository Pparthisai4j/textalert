package com.nccss.acts.textalertbatch;

 
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TextAlertBatchApplication {

	public static void main(String[] args) {
		SpringApplication.run(TextAlertBatchApplication.class, args);
	}

   
}
