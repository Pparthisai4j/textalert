package com.nccss.acts.textalertbatch.config;

 

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.DefaultBatchConfigurer;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
 
import com.nccss.acts.textalertbatch.core.MessageFromDBReader;
import com.nccss.acts.textalertbatch.core.SendMessageProcessor;
import com.nccss.acts.textalertbatch.core.UpdateMessageStatus;
import com.nccss.acts.textalertbatch.model.CodeLookUp;

@Configuration
@EnableBatchProcessing
public class TextAlertBatchConfig extends DefaultBatchConfigurer {
		
	/*	@Value("classpath:schema-drop-db2.sql")
		private Resource dropReopsitoryTables;

		@Value("classpath:schema-db2.sql")
		private Resource dataReopsitorySchema; */
    
	    @Autowired 
	    private JobBuilderFactory jobs;

	    @Autowired 
	    private StepBuilderFactory steps;
	    
	    @Bean
	    public ItemReader<CodeLookUp> itemReader() {
	        return new MessageFromDBReader();
	    }
	    
	    @Bean
	    public ItemProcessor<CodeLookUp, CodeLookUp> itemProcessor() {
	        return new SendMessageProcessor();
	    }
	    
	    @Bean
	    public ItemWriter<CodeLookUp> itemWriter() {
	        return new UpdateMessageStatus();
	    }
	    
	    @Bean
	    protected Step processLines(ItemReader<CodeLookUp> reader,
	      ItemProcessor<CodeLookUp, CodeLookUp> processor, ItemWriter<CodeLookUp> writer) {
	        return steps.get("processLines").<CodeLookUp, CodeLookUp> chunk(2)
	          .reader(reader)
	          .processor(processor)
	          .writer(writer)
	          .build();
	    }
	    
	    @Bean
	    public Job job() {
	        return jobs
	          .get("REST-API-JOB")
	          .start(processLines(itemReader(), itemProcessor(), itemWriter()))
	          .build();
	    }
	  /*  
	    @Bean
	    public DataSourceInitializer dataSourceInitializer(DataSource dataSource)
	      throws MalformedURLException {
	    	
	    	ResourceDatabasePopulator databasePopulator = new ResourceDatabasePopulator();
	    	databasePopulator.addScript(dropReopsitoryTables);
	        databasePopulator.addScript(dataReopsitorySchema);
	        databasePopulator.setIgnoreFailedDrops(true);
	           
	    	DataSourceInitializer initializer = new DataSourceInitializer();
	        initializer.setDataSource(dataSource);
	        initializer.setDatabasePopulator(databasePopulator);

	        return initializer;
	    }*/
	    
}
